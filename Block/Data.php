<?php

namespace Grudado\Trustvox\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Grudado\Trustvox\Helper\Config;

/**
 * Class Data
 * @package Grudado\Trustvox\Block
 */
class Data extends \Magento\Framework\View\Element\Template
{
    /**
     * @param Template\Context $context
     */
    protected $helper;

    public function __construct(\Magento\Catalog\Block\Product\Context $productContext,
                                Context $context, Config $helper) {
        $this->_imageBuilder = $productContext->getImageBuilder();
        $this->_coreRegistry = $productContext->getRegistry();
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    /**
     * Retrieve current product model
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getSku($product)
    {
        $sku = $this->$product->getSku();
        return $sku;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->helper->isActive();
    }
    /**
     * @return string
     */
    public function getstoreId() {
        return $this->helper->getstoreId();
    }
    /**
     * @return string
     */
    public function getToken() {
        return $this->helper->getToken();
    }
    /**
     * @return bool
     */
    public function isProduction() {
        return $this->helper->isProduction();
    }

}