<?php
namespace Grudado\Trustvox\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
/**
 * Class Config
 * @package Grudado\Trustvox\Helper
 */
class Config extends AbstractHelper
{
//    const CONFIG_DATA_ACTIVE = 'grudado_trustvox/configuration/active';
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }
    /**
     * @return bool
     */
    public function isActive($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->scopeConfig->getValue('grudado_trustvox/configuration/active', $scope);
    }
    /**
     * @return string
     */
    public function getstoreId($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->scopeConfig->getValue('grudado_trustvox/configuration/store_id', $scope);
    }
    /**
     * @return string
     */
    public function getToken($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->scopeConfig->getValue('grudado_trustvox/configuration/token', $scope);
    }
    /**
     * @return bool
     */
    public function isProduction($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT) {
        return $this->scopeConfig->getValue('grudado_trustvox/configuration/production', $scope);
    }

}
